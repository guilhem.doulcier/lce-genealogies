import glob
import os
import utils
import datetime
from collections import namedtuple

notebooks = list(sorted(glob.glob("*.ipynb")))
JUPYTER_CMD = 'python3 -m nbconvert --execute --inplace {notebook}' 

Artefact = namedtuple("Artifact", ("path", "task", "target", "title", "description"))
output = ([Artefact("02_tree_display/survival_phase12_bar.pdf", "plot_genealogies", "fig",
                    "Figure 2", "Proportion of surviving collectives in phase I and II for each cycle"),
           Artefact("02_tree_display/all_genealogies.pdf", "plot_genealogies", "fig",
                    "Figure 3", "Lineage genealogies"),
           Artefact("03_survival/llcsm_improvement.pdf", "plot_survival", "fig",
                    "Figure 4", "Nodes in the L-LCS- genealogy predicted to mark adaptive change"),
           Artefact("05_mutation_propagation/nmut_colored_by_mut.pdf", "plot_propagation", "fig",
                    "Figure 5", "Reconstruction of the number of mutations through time"),
           Artefact("05_mutation_propagation/mut-L-LCS--tree.pdf","plot_propagation","fig",
                    "Figure 6", "mutL and mutS mutations in the L-LCS- genealogy"),
           Artefact(f"03_survival/all_improvement.pdf", "plot_survival", "supfig",
                    "Improvement all lineages", "Nodes in all lineages predicted to mark adaptive change"),
           Artefact(f"06_fitness_assays/proba.pdf", "plot_fitness", "fig",
                    "Figure 7", "Probability and fitness estimate"),
           Artefact(f"04_mutations/common_mutations.html", "parse_sequencing", "table",
                    "Common mutations", "List of mutations identified in at least one ancestral genotype"),
           Artefact(f"04_mutations/sequencing.html", "parse_sequencing", "table", "Sequencing",
                    "Metadata for each sequenced genotype"),
           Artefact(f"04_mutations/sequenced_mut_mutations.pdf","parse_sequencing","supfig",
                    "MMR system mutations", "Identified mutations of the methyl-directed mismatch repair system in the sequenced genomes"),
           Artefact(f"04_mutations/number_of_mutations.pdf","parse_sequencing","supfig",
                    "Number of mutations", "Estimated number of identified by direct sequencing."),
           Artefact(f"05_mutation_propagation/nmut_colored_by_exp.pdf","plot_propagation","supfig",
                    "Number of mutations reconstructed by belief propagation.", "Estimated number of mutation reconstructed by belief propagation."),
           Artefact(f"05_mutation_propagation/mutations_tree.pdf","plot_propagation","supfig",
                    "Number of mutations within the genealogies", "Estimated number of mutation reconstructed by belief propagation displayed on the genealogies."),
           Artefact(f"05_mutation_propagation/nmut-L-LCS--tree.pdf","plot_propagation","supfig",
                    "Mutations on L-LCS- tree", "Mut mutations and number of mutations."),
           Artefact(f"05_mutation_propagation/nmut-L-LCS+-tree.pdf","plot_propagation","supfig",
                    "Mutations on L-LCS+ tree", "Mut mutations and number of mutations."),
           Artefact(f"05_mutation_propagation/nmut-S-LCS--tree.pdf","plot_propagation","supfig",
                    "Mutations on S-LCS- tree", "Mut mutations and number of mutations."),
           Artefact(f"05_mutation_propagation/nmut-S-LCS+-tree.pdf","plot_propagation","supfig",
                    "Mutations on S-LCS+ tree", "Mut mutations and number of mutations."),
           Artefact(f"05_mutation_propagation/mutL_endpoints.pdf","plot_propagation","supfig",
                    "MutL mutation origin using only the sequenced endpoints", "Mut mutations and number of mutations."),
           Artefact(f"05_mutation_propagation/mutL.pdf","plot_propagation","supfig",
                    "MutL mutation origin using all sequencing data", "Mut mutations and number of mutations."),
           Artefact(f"07_compare_fit/comparison_llcm_p1.pdf","comparison","supfig",
                    "Comparison of Ph. I survival", "Between colgen and fitness assays."),
           Artefact(f"06_fitness_assays/proba_S-LCS+.pdf","plot_fitness","supfig",
                    "Probabilities in S-LCS+ endpoints", "Results of fitness assays."),
           Artefact(f"06_fitness_assays/proba_S-LCS-.pdf","plot_fitness","supfig",
                    "Probabilities in S-LCS- endpoints", "Results of fitness assays."),
           Artefact(f"06_fitness_assays/proba_L-LCS+.pdf","plot_fitness","supfig",
                    "Probabilities in L-LCS+ endpoints", "Results of fitness assays."),
           Artefact(f"06_fitness_assays/proba_L-LCS-.pdf","plot_fitness","supfig",
                    "Probabilities in L-LCS- endpoints", "Results of fitness assays."),
           ])
targets = lambda task: [o.path for o in output if o.task==task] 
output_folder = "www"

def make_index(output, output_folder):
    header = """<html>
  <head>
    <title>Life cycle experiment data</title>
    <style type="text/css">
      body {
	  max-width:800px;
	  margin: auto;
      }
    </style>
  </head>
  <body>
    <h1>Life Cycle Experiment</h1>
    Data and code from "<em>Evolutionary dynamics of nascent multicellular lineages</em>" (2024),<br/> Doulcier G, Remigi P, Rexin D and Rainey PB. 
    """
    content = []
    content.append('<ul>')
    content.append('<li><a href="interactive/">Interactive Colgen Output</a> Displays genealogies and propagated mutation information.</li>')
    content.append('<li><a href="lce_data.sqlite">Full dataset as a SQLite database (33MB)</a></li>')
    content.append('<li><a href="https://doi.org/10.5281/zenodo.11170876">&#x1f517; Zenodo Record 11170876</a></li>')
    content.append('<li><a href="https://gitlab.gwdg.de/guilhem.doulcier/lce-genealogies/">&#x1f517; Code repository on gitlab</a></li>')
    content.append('<li><a href="https://doi.org/10.1101/2024.05.10.593459">&#x1f517; Preprint on Biorxiv</a></li>')
    content.append('<li><a href="https://zenodo.org/records/11174902">&#x1f517; Colgen</a></li>')


    content.append('</ul>')

    title = {"table":"Tables", "fig":"Figures", "supfig":"Supplementary Figures"}
    for kind in ["table","fig","supfig"]:
        content.append(f"<h2>{title[kind]}</h2><ul>")
        content += [f'<li><a href="{kind}/{os.path.basename(o.path)}">{o.title}</a> {o.description}</li>'
                    for o in output if o.target==kind]
        content += ["</ul>"]
    content.append(f"<h2>Notebooks</h2><ul>")
    content.append("All outputs are generated by running the following Python notebooks:")
    content += [f'<li><a href="notebooks/{os.path.basename(n).replace("ipynb","html")}">{os.path.basename(n)}</a></li>'
                    for n in sorted(notebooks)]
    content += ["</ul>"]
    
    footer = f"<footer>Generated on {datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')}</footer></body></html>"
    with open(os.path.join(output_folder, 'index.html'),'w') as file:
        file.write('\n'.join([header]+content+[footer]))
        
def task_gather():
    return {
        "file_dep":[o.path for o in output],
        "targets": [os.path.join(output_folder, o.target, os.path.basename(o.path)) for o in output]+[os.path.join(output_folder,'index.html')],
        'task_dep':[o.task for o in output],
        "actions":([f"mkdir -p {os.path.join(output_folder, o.target)}" for o in output]
                   +[f"cp {o.path} {os.path.join(output_folder, o.target,'')}" for o in output]
                   +[f"cp lce_data.sqlite {output_folder}/"]
                   +[(make_index, (output, output_folder))]
                   +['rm -rf public']
                   +['mv www public']
                   +["tar -czvf public.tar.gz public"]
                   +['mv public www'])
    }
    
def task_document():
    """ Document the tools used to generate the results """ 
    return {
        "file_dep":[],
        "targets":["python-env.txt"],
        "actions":["pip freeze > python-env.txt",
                   "cp python-env.txt www/python-env.txt"]
    }

def task_parse_genealogies():
    return {
        "file_dep": ["raw/19-09-27/Counts_final.xlsx"],
        "targets": (utils.DATASETS
                    +[x.replace(".csv", "_p1.csv") for x in utils.DATASETS]
                    +[x.replace(".csv", "_p2.csv") for x in utils.DATASETS]),
        'actions': [JUPYTER_CMD.format(notebook="01_genealogies.ipynb") ],
        'clean': True,
    }

def task_plot_genealogies():
    return {
        "file_dep": utils.DATASETS,
        "targets": targets('plot_genealogies'),
        'actions': [JUPYTER_CMD.format(notebook="02_tree_display.ipynb") ],
        'clean': True,
    }

def task_plot_survival():
    return {
        "file_dep": (utils.DATASETS+
                     [f"03_survival/{x}-modelbeta-grid100-p1-1500.csv" for x in utils.TREATMENTS]+
                     [f"03_survival/{x}-modelbeta-grid100-p2-1500.csv" for x in utils.TREATMENTS]),
        "targets": targets('plot_survival'),
        'actions': [JUPYTER_CMD.format(notebook="03_survival.ipynb") ],
        'clean': True,
    }

def task_parse_sequencing():
    return {
        "targets": ([f"04_mutations/trees/{x}_mutations.csv" for x in utils.TREATMENTS]
                     +["04_mutations/number_of_mutations.pdf", "04_mutations/sequenced_cultures.csv"]),
        "file_dep": ["raw/24-03-22/Breseq/17_L/17_A1/data/summary.json"],
        'task_dep':['parse_genealogies', ],
        'actions': [JUPYTER_CMD.format(notebook="04_sequencing.ipynb")],
        'clean': True,
    }
 

def task_plot_propagation():
    return {
        "file_dep": ([f"04_mutations/propagated/{x}_mutation_report.csv" for x in utils.TREATMENTS]
                    +[f"04_mutations/propagated/{x}_mutation_mapping.csv" for x in utils.TREATMENTS]),
        "targets": targets("plot_propagation"),
        'task_dep':['mutation_propagation'],
        'actions': [JUPYTER_CMD.format(notebook="05_mutation_propagation.ipynb")],
        'clean': True,
    }

def task_plot_fitness():
    return {
        "targets": (["06_fitness_assays/fitness_assays.csv"]+targets("plot_fintess")),
        "file_dep": ["raw/24-04-03/Lifecycle fitness tests.xlsx"],
        'actions': [JUPYTER_CMD.format(notebook="06_fitness_assays.ipynb")],
        'clean': True,
    }

def task_comparison():
    return {
        "file_dep": utils.DATASETS+['06_fitness_assays/fitness_assays.csv'],
        'task_dep':['plot_fitness','plot_survival'],
        "targets": targets('comparison'),
        'actions': [JUPYTER_CMD.format(notebook="07_compare_fit.ipynb") ],
        'clean': True,
    }

def task_html():
    return {
        "file_dep": [],
        "targets": [],
        'task_dep':['parse_genealogies', ],
        "actions": ([f'mkdir -p {os.path.join(output_folder,"notebook")}']
                    +[f'jupyter nbconvert --execute --to html --output-dir="www/notebooks" {x}'
                      for x in notebooks])
    }

def task_clear_output():
    return {
        "file_dep": [],
        "targets": [],
        'task_dep':[],
        "actions": [f"jupyter nbconvert --clear-output --inplace {x}" for x in notebooks]
    }
           
grids = [100]
models = ['jump', 'beta']
minmax = {
    'jump': {"01": [0, 1, 0.5]},
    'beta': {"500": [1, 500, 2],
             "100": [1, 100, 2],
             "1500": [1, 1500, 2]}
          }
def task_survival():
    """Use Colgen to estimate survival"""
    for n,dt in zip(utils.TREATMENTS, utils.DATASETS):
        for p in (1,2):
            input_file = dt.replace(".csv", f"_p{p}.csv")
            for g in grids:
                for m in models:
                    for k, mm in minmax[m].items():
                        out = f"03_survival/{n}-model{m}-grid{g}-p{p}-{k}.csv"
                        yield {
                            "file_dep": [input_file],
                            "targets": [out,
                                        out.replace("csv","convergence")],
                            "name": out,
                            "actions": [
                                "mkdir -p 03_survival/", 
                                f"colgen survival --model {m} --steps {g} "
                                f"--sigma_max {mm[1]} --sigma_min {mm[0]} --sigma0 {mm[2]} "
                                f"--outpath {out} {input_file}"]
                        }


def task_mutation_propagation():
    """Use colgen to propagate mutational data"""
    for n,dt in zip(utils.TREATMENTS, utils.DATASETS):
        for suffix in ["_mutations.csv","_mutations_endpoints.csv"]:
            mutations = dt.replace(".csv", suffix).replace("01_genealogies/", "04_mutations/trees/")
            outdir = "04_mutations/propagated/endpoints" if "endpoints" in suffix else "04_mutations/propagated"
            yield {
                "file_dep": [dt, mutations],
                "targets": [os.path.join(outdir, f"{n}_mutation_mapping.csv"),
                            os.path.join(outdir, f"{n}_mutation_report.csv")],
                "name": n+suffix,
                "actions": [
                    f"mkdir -p {outdir}", 
                    (f"colgen mutation "
                     f"{dt} "
                     f"{mutations} "
                     f"--mutation_description 04_mutations/mutation_description.csv "
                     f"--out_dir {outdir}")]
            }



def prepare_interactive(folder):
    import sqlite3
    import pandas as pd
    with sqlite3.connect('lce_data.sqlite') as db:
        exp_list = db.execute("SELECT DISTINCT experiment FROM genealogies").fetchall()
        for exp, in exp_list:
            g = pd.read_sql_query(
    f"""SELECT * FROM genealogies 
    LEFT JOIN (SELECT name, COUNT(*) number_of_mutations 
    FROM propagated_mutations GROUP BY propagated_mutations.name) n 
    ON n.name = genealogies.name
    WHERE experiment=='{exp}'"""
                                  , db)
            g = g.loc[:,~g.columns.duplicated()].copy()
            s = pd.read_sql_query(f"SELECT * FROM survival WHERE experiment=='{exp}' AND model=='beta' AND fitname=='1500'", db)
            s['survival_name'] = [f"survival_estimate" for _,row in s.iterrows()]
            s['survival_change_name'] = [f"survival_change" for _,row in s.iterrows()]
            pivot = s.loc[['ROOT' not in n for n in s.name],:].pivot(values='survival_estimate', index='name', columns='survival_name')
            pivot2 = s.loc[['ROOT' not in n for n in s.name],:].pivot(values='survival_change', index='name', columns='survival_change_name')
            m = pd.merge(g, pivot.reset_index(),left_on='name',right_on='name')
            m = pd.merge(m, pivot2.reset_index(),left_on='name',right_on='name')
            m.to_csv(os.path.join(folder,exp+".csv"))

def task_interactive():
        return {
        "file_dep": [],
        "targets": [],
        'task_dep':['gather'],
            "actions": (["mkdir -p www/interactive"]
                        +[(prepare_interactive, ['www/interactive'])]
                        +['cp 04_mutations/propagated/*report.csv www/interactive']
                        +["colgen interactive www/interactive/*.csv  --path www/interactive --batch"])
    }


