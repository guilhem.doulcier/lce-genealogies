# Code, data and figures for ""Evolutionary dynamics of nascent multicellular lineages" 

This repository contains the code and data to reproduce the results and figures of the article "Evolutionary dynamics of nascent multicellular lineages". 

All raw the datasets are available in the `raw` repository.

To reproduce all the processed datasets and figures from the article, you have to install the dependencies listed in `python-env.txt` and then run the `doit` command in the repository.

Example:

```
cd lce-genealogies 
python3 -m venv venv
source venv/bin/activate
pip install -r python-env.txt
doit
```

A global output in HTML format will be produced in `www`. 

A copy of the output is saved in `public.tar.gz` and served on https://lce-genealogies-guilhem-doulcier-3738d8bfef5fc3d36107490504a36f.pages.gwdg.de
 
## References 
- Doulcier, G., Remigi P., Rexin R., Rainey P.B., "Evolutionary dynamics of nascent multicellular
 lineages", Bioarxiv, 2024. https://doi.org/10.1101/2024.05.10.593459
- Doulcier, G., Remigi P., Rexin R., Rainey P.B., "Code and Data for Evolutionary dynamics of nascent multicellular lineages", Zenodo, 2024. https://doi.org/10.5281/zenodo.11170876
- Doulcier, G. “Colgen -  Collective Genealogies Visualisation and Analysis.” Zenodo, 2019. https://doi.org/10.5281/zenodo.7342768.
