#!/usr/bin/env bash
git clone git@gitlab.gwdg.de:guilhem.doulcier/lce-genealogies.git
cd lce-genealogies 
python3 -m venv venv
source venv/bin/activate
pip install -r python-env.txt
doit -n 4
