TREATMENTS = ["S-LCS+","L-LCS+","S-LCS-","L-LCS-"]
DATASETS = [f"01_genealogies/{x}.csv" for x in TREATMENTS]
COLORS = {"S-LCS+":"C0","L-LCS+":"C1","S-LCS-":"C2","L-LCS-":"C3"}

def colorize_pondscum(d):
    if (d.phase == 1 and d.mat==0):
        return 'C1'
    elif (d.phase == 1 and d.mat == 1 and d.count_sm == 0):
        return 'C3'
    elif (d.phase == 2 and d.count_ws == 0):
        return 'C2'
    else:
        return 'C0'

def rack_layout(dataframe, rack_order="bcdefa"):
    """Genealogies with the Rack Layout"""
    pos = {}
    if rack_order is None:
        rack_order = [x.lower() for x in sorted(dataframe.rack.unique())]
    for _,d in dataframe.iterrows():
        pos[d['name']] = (rack_order.index(d.rack.lower()))*20 + int(d.tube[-1])*2
    return pos

def format_tree_labels(keys, ax, scales):
    xticks = {scales[0](k):'-'.join(k.split('_')[1].split('-')[:2]) for k in keys}
    ax.set_xticks(*zip(*xticks.items()))
    yticks = {scales[1](k):k.split('_')[1].split('-')[-1] for k in keys}
    ax.set_yticks(*zip(*yticks.items()))
